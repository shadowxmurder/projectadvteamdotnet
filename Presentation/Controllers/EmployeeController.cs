﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Net.Http;
using Domain.Entity;

namespace Presentation.Controllers
{
    public class EmployeeController : Controller
    {
        // GET: Employee
        public ActionResult PrintEmp()
        {
            HttpClient Client = new System.Net.Http.HttpClient();
            //Client.BaseAddress = new Uri("http://localhost:9080");
            Client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = Client.GetAsync("http://localhost:9080/ProjetPidevJee-web/rest/Employee/GetEmployee").Result;
            if (response.IsSuccessStatusCode)

            {
                ViewBag.result = response.Content.ReadAsAsync<IEnumerable<employee>>().Result;


            }
            else
            {
                ViewBag.result = "error";
            }

            return View();
        }

        // GET: Employee/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
       /* public ActionResult Create(Usert us)
        {andro gn


            HttpClient Client = new HttpClient();
            HttpResponseMessage response = Client.PostAsJsonAsync<Usert>(" http://localhost:9080/pidev-web/api/users/addUser", us).ContinueWith((postTask) => postTask.Result.EnsureSuccessStatusCode()).Result;
            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }


            {
                return View();
            }
        }*/
        // GET: Employee/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Employee/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Employee/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Employee/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Employee/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Employee/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
