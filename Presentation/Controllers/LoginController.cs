﻿using System.Web.Mvc;
using Domain.Entity;
using System.Net.Http;
using System.Web;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System;
using CaptchaMvc.HtmlHelpers;
namespace Presentation.Controllers
{
    public class LoginController : Controller
    {
        public static employee currentUser;
        //   private static employee objemp;




        // GET: Login

        //public ActionResult Login(employee user)
        //{
        //    HttpClient Client = new System.Net.Http.HttpClient();
        //    //Client.BaseAddress = new Uri("http://localhost:9080");
        //    Client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

        //    HttpResponseMessage response = Client.GetAsync("http://localhost:9080/ProjetPidevJee-web/rest/Employee/getLogin" + user.email + "/" + user.password).Result;

        //     var s = response.Content.ReadAsAsync<employee>();


        //if (user.email == null || user.password == null)

        //                {
        //        //return RedirectToAction("PrintEmp", "Employee");
        //        return View();
        //    }
        //                else if (response.IsSuccessStatusCode)


        //                {
        //         employee emp = new employee();
        //         emp.email = user.email;
        //         emp.password = user.password;
        //         emp.Lname = s.Result.Lname;
        //         emp.Fname = s.Result.Fname;
        //         emp.id = s.Result.id;

        //        Session.Add("CurrentUser", emp);
        //        // Session["EmployeeSessionData1"] = emp;
        //      //  TempData["EmployeeTempData"] = emp;
        //        //Console.WriteLine("logged!!");
        //        return RedirectToAction("PrintEmp","Employee");
        //    }
        ///*
        //else if (response.IsSuccessStatusCode && data.Result.role == "Employee" && data.Result.isActif == true)
        //{

        //    return RedirectToAction("Index");

        //}*/
        //return RedirectToAction("PrintEmp", "Employee");


        //}
        public ActionResult Login(employee u)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            if (!this.IsCaptchaValid(""))
            {
                ViewBag.error = "Captcha Invalide !";
                return View();
            }
            else
            {

                var request = (HttpWebRequest)WebRequest.Create("http://localhost:9080/ProjetPidevJee-web/rest/Employee/getLogin/" + u.email + "/" + u.password);

                request.Method = "GET";

                request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                var content = string.Empty;
                try
                {

                    var xRES = (HttpWebResponse)request.GetResponse();
                    using (var response = (HttpWebResponse)request.GetResponse())
                    {
                        using (var stream = response.GetResponseStream())
                        {
                            using (var sr = new StreamReader(stream))
                            {
                                content = sr.ReadToEnd();
                            }
                        }
                    }


                    if (content == null)
                    {

                        return View();

                    }
                    else
                    {
                        try
                        {
                            JObject json = JObject.Parse(content);


                            var result = JsonConvert.DeserializeObject<employee>(content);
                            currentUser = result;




                            Session["EmployeeSessionData1"] = currentUser;




              //              return RedirectToAction("PrintEmp", "Employee");

                      return RedirectToAction("allPub", "Publication");



                        }
                        catch (Exception ex)
                        {


                            return View();
                        }
                    }
                }
                catch (WebException ex)
                {

                    return View();
                }


            }




        }
        // GET: Login/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Login/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Login/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Login/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Login/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Login/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Login/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
