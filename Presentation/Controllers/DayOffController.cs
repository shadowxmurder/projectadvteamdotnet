﻿using Domain.Entity;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Presentation.Controllers
{
    public class DayOffController : Controller
    {
        IDayOffService ds;
        public DayOffController()
        {
            ds = new DayOffService();
        }
        // GET: DayOff
        public ActionResult Index()
        {
            return View(ds.GetMany());
        }
        public ActionResult publication()
        {
            return View();
        }
        // GET: DayOff/Details/5
        public ActionResult Details(int id)
        {
            dayoff c = ds.GetById(id);
            if (c == null)
            {

                return HttpNotFound();

            }

            return View(c);
        }

        // GET: DayOff/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DayOff/Create
        [HttpPost]
        public ActionResult Create(dayoff day)
        {
               ds.Add(day);
            ds.Commit();
            //  return RedirectToAction("index");
            return RedirectToAction("Index");
        }

        // GET: DayOff/Edit/5
        public ActionResult Edit(int id)
        {

            dayoff c = ds.GetById(id);

            if (c == null)
            {
                return HttpNotFound();
            }
            return View(c);
        }

        // POST: DayOff/Edit/5
        [HttpPost]
        public ActionResult Edit(dayoff day)
        {
            dayoff c1 = ds.GetById(day.id);
            c1.nbjrs = day.nbjrs;

            if (ModelState.IsValid)
            {
                ds.Update(c1);
                ds.Commit();

                return RedirectToAction("Index");

            }

            return RedirectToAction("Index");
        }

        // GET: DayOff/Delete/5
        public ActionResult Delete(int id)
        {
            return View(ds.GetById(id));
        }

        // POST: DayOff/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, dayoff day)
        {
            day = ds.GetById(id);
            ds.Delete(day);
            ds.Commit();
            return RedirectToAction("Index");
        }
        //public ActionResult Pdf()
        //{
        //    return new ActionAsPdf("Index")
        //    {
        //        FileName = Server.MapPath("~/Content/CRM.Pdf")

        //    };
        //}
    }
}
