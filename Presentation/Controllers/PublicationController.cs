﻿using Data;
using Domain.Entity;
using Presentation.Controllers;
using Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

using System.Web.Mvc;

namespace Web.Controllers
{
    public class PublicationController : Controller
    {
        private Context db = new Context();

        IPublicationService publicationService;
        IFeedbackService fb;
        ILikeService fl;


        //  ILikeService lp;
        public PublicationController()
        {
          //  lp = new LikeService();
            publicationService = new PublicationService();
            fb = new FeedbackService();
            fl = new LikeService();


        }
        // GET: Publication
        public ActionResult allPub()
        {
            ViewBag.result = (publicationService.GetAll().OrderByDescending(x => x.date)); ;
            
            return View();
        }
        

        // GET: Publication/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Publication/Create
        
        //  POST: Publication/Create
        [HttpPost]
        public ActionResult Create(publication pu, HttpPostedFileBase file1)
        {
            String chain = pu.statut;
            String mot = "rouge";
            String mot1 = "bleu";
            String mot2 = "vert";
            if (chain.Contains(mot))
            {
                chain = chain.Replace(mot, "*****");
                pu.statut = chain;

            }
            if (chain.Contains(mot1))
            {
                chain = chain.Replace(mot1, "*****");
                pu.statut = chain;

            }
            if (chain.Contains(mot2))
            {
                chain = chain.Replace(mot2, "*****");
                pu.statut = chain;

            }



            pu.user_id = LoginController.currentUser.id;
            pu.date = DateTime.Now;
           
            try
            {
                if (file1.ContentLength > 0)
                {
                    pu.file = file1.FileName;

                    var path = Path.Combine(Server.MapPath("~/Content/upload/"), file1.FileName);
                    file1.SaveAs(path);
                }
                publicationService.Add(pu);

                publicationService.Commit();
                return RedirectToAction("allPub", "Publication");
            }
            catch (Exception ex)
            {

                publicationService.Add(pu);

                publicationService.Commit();
                return RedirectToAction("allPub", "Publication");
            }
          
        }
        public ActionResult Like(int idp)
        {
            int fazaLike = fl.LikeExist(idp, LoginController.currentUser.id);
            try
            {
                if (fazaLike == 0)
                {
                    reactp r = new reactp();
                    r.publication_id = idp;
                    r.user_id = LoginController.currentUser.id;
                  //db.users.Find(LoginController.currentUser.id).badge = "kjqsgkjfs";

                    fl.Add(r);
                    fl.Commit();
                    return RedirectToAction("allPub", "Publication");
                }
                else
                    return null ;
            }
            catch (Exception ex)
            {
               
                return RedirectToAction("allPub", "Publication");
            }
            return RedirectToAction("allPub", "Publication");

        }

        // GET: Comment/Edit/5
        public ActionResult Edit(int id)
        {
            publication d = publicationService.GetById(id);
            return View(d);
        }







        // POST: Comment/Edit/5

        //[HttpPost]
        //public ActionResult Edit(int id, publication dep)
        //{
        //    try
        //    {
        //        // TODO: Add update logic here
        //        publication d = publicationService.GetById(id);
        //        d.statut = dep.statut;
        //        d.file = dep.file;
        //        d.UpdatePublication(d);

        //        return RedirectToAction("Index", "DepotFront");

        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}


        // GET: Publication/Delete/5
        public ActionResult Delete(int id)
        {
            var r = publicationService.GetById(id);
          //  List<feedback> = db.feedbacks.Single(s => s.publication_id.Equals(id));
            publicationService.Delete(r);
            publicationService.Commit();
            return RedirectToAction("allPub", "Publication");
        }

        // POST: Publication/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }

}
