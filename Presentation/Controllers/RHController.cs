﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Entity;
using System.Net.Http;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Twilio.AspNet.Mvc;
using Twilio;
using Twilio.TwiML;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using System.Configuration;

namespace Presentation.Controllers
{
    public class RHController : Controller
    {
        public static humanressources currentrh;
        // GET: RH
        public static humanressources hr = new humanressources
        { email = "advteam@esprit.tn",
            password = "123"
        };

        public ActionResult RHlogin(humanressources hr)
        { if (!ModelState.IsValid)
            {
                return View();
            }

            else
            {

                var request = (HttpWebRequest)WebRequest.Create("http://localhost:9080/ProjetPidevJee-web/rest/Employee/LogHr/" + hr.email + hr.password);

                request.Method = "GET";

                request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                var content = string.Empty;
                try
                {

                    var xRES = (HttpWebResponse)request.GetResponse();
                    using (var response = (HttpWebResponse)request.GetResponse())
                    {
                        using (var stream = response.GetResponseStream())
                        {
                            using (var sr = new StreamReader(stream))
                            {
                                content = sr.ReadToEnd();
                            }
                        }
                    }


                    if (content == null)
                    {

                        return View();

                    }
                    else
                    { return RedirectToAction("PrintEmp", "Employee");
                        try
                        {
                            JObject json = JObject.Parse(content);


                            var result = JsonConvert.DeserializeObject<humanressources>(content);
                            currentrh = result;




                            Session["RHSessionData1"] = currentrh;




                            //              return RedirectToAction("PrintEmp", "Employee");

                            return RedirectToAction("PrintEmp", "Employee");



                        }
                        catch (Exception ex)
                        {


                            return View();
                        }
                    }
                }
                catch (WebException ex)
                {

                    return View();
                }


            }




        }

        // GET: RH/Details/5
        public ActionResult PasswordForget()
        {
            return View();
           

        }

        // GET: RH/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: RH/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: RH/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: RH/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: RH/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: RH/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
} 