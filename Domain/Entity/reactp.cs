namespace Domain.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("projetpidevjee.reactp")]
    public partial class reactp
    {
        public int id { get; set; }

        public int? publication_id { get; set; }

        public int? user_id { get; set; }

        public virtual employee employee { get; set; }

        public virtual publication publication { get; set; }
    }
}
