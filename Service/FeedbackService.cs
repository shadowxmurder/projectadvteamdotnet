﻿using Data.Infrastructure;
using Domain.Entity;
using ServicePattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class FeedbackService : Service<feedback>, IFeedbackService
    {
        static IDatabaseFactory Factory = new DatabaseFactory();
        static IUnitOfWork iow = new UnitOfWork(Factory);
        public FeedbackService() : base(iow)
        {

        }
        public  int nb_Comment( int idp)
        {
            IEnumerable<feedback> fact = GetAll().Where(x => x.publication_id.Equals(idp));

            return fact.Count();

            
        }
    }
}
