﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ServicePattern;

using Domain.Entity;
using Data.Infrastructure;

namespace Service
{
   public  class DayOffService: Service<dayoff>,IDayOffService

    {
  
        private static IDatabaseFactory dbfactor = new DatabaseFactory();
            private static IUnitOfWork wow = new UnitOfWork(dbfactor);

            public DayOffService() : base(wow)
            {

            }
        
    }
}
